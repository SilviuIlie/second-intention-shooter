// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "STrackerBot.generated.h"

class UHealthComponent;
class USphereComponent;
class USoundCue;


UCLASS()
class SECONDINTENTION_API ASTrackerBot : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASTrackerBot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector GetNextPathPoint();

	void OnNearbyBots();

	void DamageSelf();
	void SelfDestruct();
	UFUNCTION()
	void OnHealthChanged(UHealthComponent* hpComponent, float health, float healthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	void RefreshPath();

	UPROPERTY(VisibleDefaultsOnly,Category = "Components")
	UStaticMeshComponent* m_meshComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	UHealthComponent* m_healthComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USphereComponent* m_sphereComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	UAudioComponent* m_audioComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
	float m_speed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
	float m_requiredDistanceToTarget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
	bool m_bAccelerationChange;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	UParticleSystem* m_explosionFX;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	float m_explosionRadius;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	float m_explosionDamage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	float m_selfDamageInterval;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	int32 m_maxPower;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sounds")
	USoundCue* m_selftDestructSound;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sounds")
	USoundCue* m_explodeSound;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sounds")
	USoundCue* m_selftDestructSound2;

	bool m_didExploded;
	bool m_didStartedSelfDestruct;
	int m_powerLevel;

	UMaterialInstanceDynamic* m_materialInstance; // dyn mat for pulse
	FVector m_nextPathPoint;

	FTimerHandle m_timerSelfDamage;
	FTimerHandle m_timerRefreshPath;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorBeginOverlap(AActor* otherActor) override;
};
