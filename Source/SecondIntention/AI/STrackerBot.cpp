// Fill out your copyright notice in the Description page of Project Settings.

#include "STrackerBot.h"
#include "Components/StaticMeshComponent.h"
#include "NavigationSystem/Public/NavigationSystem.h"
#include "NavigationSystem/Public/NavigationPath.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "Public/HealthComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "DrawDebugHelpers.h"
#include "Components/SphereComponent.h"
#include "Public/Player/RPGCharacter.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Public/TimerManager.h"

static int32 debugTrackerBotDrawing = 0;
static FAutoConsoleVariable CVARDebugWeaponDrawing(TEXT("Shooter.DebugTrackerBot"), debugTrackerBotDrawing, TEXT("Draw Debug Lines For TrackerBot"), EConsoleVariableFlags::ECVF_Cheat);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ASTrackerBot::ASTrackerBot()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	m_meshComponent->SetCanEverAffectNavigation(false);
	m_meshComponent->SetSimulatePhysics(true);
	RootComponent = m_meshComponent;

	m_healthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	m_healthComponent->m_onHealthChanged.AddDynamic(this, &ASTrackerBot::OnHealthChanged);

	m_sphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	m_sphereComponent->SetSphereRadius(200);
	m_sphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	m_sphereComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	m_sphereComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	m_sphereComponent->SetupAttachment(RootComponent);

	m_audioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio Component"));
	m_audioComponent->SetupAttachment(RootComponent);

	m_didStartedSelfDestruct = false;
	m_didExploded = false;
	m_bAccelerationChange = false;
	m_speed = 1000.f;
	m_requiredDistanceToTarget = 50.f;
	m_explosionDamage = 40.f;
	m_explosionRadius = 200.f;
	m_selfDamageInterval = 0.5f;
	m_maxPower = 4;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASTrackerBot::BeginPlay()
{
	Super::BeginPlay();
	
	if (Role == ROLE_Authority)
	{
		m_nextPathPoint = GetNextPathPoint();

		FTimerHandle timerSetPowerLevel;
		GetWorldTimerManager().SetTimer(timerSetPowerLevel, this, &ASTrackerBot::OnNearbyBots, 1.f, true);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FVector ASTrackerBot::GetNextPathPoint()
{
	float smallestDistance = FLT_MAX;
	AActor* bestTarget = nullptr;
	for (FConstPawnIterator it = GetWorld()->GetPawnIterator(); it; ++it)
	{
		APawn* pawn = it->Get();
		if (pawn == nullptr || UHealthComponent::IsFriendly(this, pawn)) continue;

		UHealthComponent* healthComponent = Cast<UHealthComponent>(pawn->GetComponentByClass(UHealthComponent::StaticClass()));
		if (healthComponent && healthComponent->GetHealth() > 0.f)
		{
			float dist = (pawn->GetActorLocation() - GetActorLocation()).Size();
			if (smallestDistance > dist)
			{
				smallestDistance = dist;
				bestTarget = pawn;
			}
		}
	}

	if (!bestTarget) return GetActorLocation();;

	UNavigationPath* path = UNavigationSystemV1::FindPathToActorSynchronously(this, GetActorLocation(), bestTarget);

	GetWorldTimerManager().SetTimer(m_timerRefreshPath, this, &ASTrackerBot::RefreshPath, .3f, false);

	if (path && path->PathPoints.Num() > 1)
	{
		return path->PathPoints[1]; // return next path point
	}
	
	return GetActorLocation();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASTrackerBot::OnNearbyBots()
{
	const float sphereRadius = 500.f;
	FCollisionShape sphereShape;
	sphereShape.SetSphere(sphereRadius);

	FCollisionObjectQueryParams queryParams;
	queryParams.AddObjectTypesToQuery(ECC_PhysicsBody);
	queryParams.AddObjectTypesToQuery(ECC_Pawn);

	TArray<FOverlapResult> overlaps;
	GetWorld()->OverlapMultiByObjectType(overlaps, GetActorLocation(), FQuat::Identity, queryParams, sphereShape);
	if (debugTrackerBotDrawing)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), sphereRadius, 10, FColor::Purple, false, 1.f);
	}
	int32 nearbyBotsCount = 0;
	for (auto overlap : overlaps)
	{
		ASTrackerBot* bot = Cast<ASTrackerBot>(overlap.GetActor());

		if (bot && bot != this)
		{
			nearbyBotsCount++;
		}
	}

	m_powerLevel = FMath::Clamp(nearbyBotsCount, 0, m_maxPower);
	if (!m_materialInstance)
	{
		m_materialInstance = m_meshComponent->CreateAndSetMaterialInstanceDynamicFromMaterial(0, m_meshComponent->GetMaterial(0));
	}
	if (m_materialInstance)
	{
		float powerLevelAlpha = m_powerLevel / (float)m_maxPower;

		m_materialInstance->SetScalarParameterValue("PowerLevelAlpha", powerLevelAlpha);
	}

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASTrackerBot::DamageSelf()
{
	UGameplayStatics::ApplyDamage(this, 20.f, GetInstigatorController(), this, nullptr);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASTrackerBot::OnHealthChanged(UHealthComponent * hpComponent, float health, float healthDelta, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	if (!m_materialInstance)
	{
		m_materialInstance = m_meshComponent->CreateAndSetMaterialInstanceDynamicFromMaterial(0, m_meshComponent->GetMaterial(0));
	}

	//check because if i don't have a material in mesh m_materialInstance will still be null
	if (m_materialInstance)
	{
		//set the bp paramater created - LastTimeDamagetaken
		m_materialInstance->SetScalarParameterValue("LastTimeDamageTaken", GetWorld()->TimeSeconds);
	}

	if (health <= 0.f)
	{
		SelfDestruct();
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASTrackerBot::RefreshPath()
{
	m_nextPathPoint = GetNextPathPoint();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASTrackerBot::SelfDestruct()
{
	if (m_didExploded)	return;

	m_didExploded = true;

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), m_explosionFX, GetActorLocation());
	
	m_meshComponent->SetVisibility(false, true);
	m_meshComponent->SetSimulatePhysics(false);
	m_meshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (Role == ROLE_Authority)
	{
		TArray<AActor*> ignoredActors;
		ignoredActors.Add(this);
		float finalDamage = m_explosionDamage + (m_explosionDamage * m_powerLevel);
		UGameplayStatics::ApplyRadialDamage(this, finalDamage, GetActorLocation(), m_explosionRadius, nullptr, ignoredActors, this, GetInstigatorController(), true);
		if (debugTrackerBotDrawing)
		{
			DrawDebugSphere(GetWorld(), GetActorLocation(), m_explosionRadius, 12, FColor::Red, false, 2.f, 0, 1.f);
		}
		SetLifeSpan(2.f); // destroy object after 2 seconds because the effects are not played on clients (Destroy deletes it immediately)
	}

	UGameplayStatics::PlaySoundAtLocation(this, m_explodeSound, GetActorLocation());
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASTrackerBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Role == ROLE_Authority && !m_didExploded)
	{
		FVector forceDirection = m_nextPathPoint - GetActorLocation();
		float distanceToTarget = forceDirection.X * forceDirection.X + forceDirection.Y * forceDirection.Y + forceDirection.Z * forceDirection.Z;

		if (distanceToTarget <= m_requiredDistanceToTarget * m_requiredDistanceToTarget)
		{
			m_nextPathPoint = GetNextPathPoint();
		}
		else
		{
			forceDirection.Normalize();
			forceDirection *= m_speed;
			m_meshComponent->AddForce(forceDirection, NAME_None, m_bAccelerationChange);
			debugTrackerBotDrawing = CVARDebugWeaponDrawing->GetInt();
			if (debugTrackerBotDrawing)
			{
				DrawDebugDirectionalArrow(GetWorld(), GetActorLocation(), GetActorLocation() + forceDirection, 32, FColor::Yellow, false, 0.f, 0, 1.f);
			}
		}

		m_audioComponent->SetVolumeMultiplier(FMath::GetMappedRangeValueClamped(FVector2D(10, 1000), FVector2D(0.1, 2), GetVelocity().Size()));

		if (debugTrackerBotDrawing)
		{
			DrawDebugSphere(GetWorld(), m_nextPathPoint, 20.f, 10, FColor::Yellow, false, 0.0f, 1.f);
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASTrackerBot::NotifyActorBeginOverlap(AActor * otherActor)
{
	Super::NotifyActorBeginOverlap(otherActor);
	if (m_didStartedSelfDestruct || m_didExploded) return;
	ARPGCharacter* playerCharacter = Cast<ARPGCharacter>(otherActor);

	if (playerCharacter && !UHealthComponent::IsFriendly(this,otherActor))
	{
		m_didStartedSelfDestruct = true;
		if (Role == ROLE_Authority)
		{
			GetWorldTimerManager().SetTimer(m_timerSelfDamage, this, &ASTrackerBot::DamageSelf, m_selfDamageInterval, true, 0.f);	
		}
		UGameplayStatics::SpawnSoundAttached(m_selftDestructSound, RootComponent);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
