// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/Pickup.h"
#include "Components/DecalComponent.h"
#include "Components/SphereComponent.h"
#include "Public/Powerup.h"
#include "Public/TimerManager.h"
#include "Public/Player/RPGCharacter.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
APickup::APickup()
{
	m_sphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	m_sphereComponent->SetSphereRadius(75.f);
	RootComponent = m_sphereComponent;
	m_decalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal Component"));
	m_decalComponent->SetRelativeRotation(FRotator(90.f, 0.f, 0.f));
	m_decalComponent->DecalSize = FVector(60, 75, 75);
	m_decalComponent->AttachTo(RootComponent);
	
	m_cooldown = 5.f;

	SetReplicates(true);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void APickup::BeginPlay()
{
	Super::BeginPlay();
	
	if (Role == ROLE_Authority)
	{
		Spawn();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void APickup::Spawn()
{
	if (!m_powerupClass)
	{
		UE_LOG(LogTemp, Warning, TEXT("Powerup class is nullptr in blueprint (object name = %s)"), *GetName());
		return;
	}
	FActorSpawnParameters spawnParameters;
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	m_powerupInstance = GetWorld()->SpawnActor<APowerup>(m_powerupClass, GetTransform(), spawnParameters);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void APickup::NotifyActorBeginOverlap(AActor * otherActor)
{
	Super::NotifyActorBeginOverlap(otherActor);

	if (Role == ROLE_Authority && m_powerupInstance)
	{
		ARPGCharacter* playerCharacter = Cast<ARPGCharacter>(otherActor);
		if (playerCharacter)
		{
			m_powerupInstance->ActivatePowerup(otherActor);
			m_powerupInstance = nullptr;

			//respawn after an amout of time
			GetWorldTimerManager().SetTimer(m_timerRespawn, this, &APickup::Spawn, m_cooldown);
		}
	}
}
