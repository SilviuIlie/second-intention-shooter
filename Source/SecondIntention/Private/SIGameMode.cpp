// Fill out your copyright notice in the Description page of Project Settings.

#include "SIGameMode.h"
#include "Public/HealthComponent.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Public/TPSPlayerState.h"
#include "EnvironmentQuery/EnvQuery.h"
#include "Public/TPSGameState.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ASIGameMode::ASIGameMode()
{
	m_timeBetweenWaves = 2.f;

	PlayerStateClass = ATPSPlayerState::StaticClass();
	GameStateClass = ATPSGameState::StaticClass();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::StartPlay()
{
	Super::StartPlay();

	PrepareNextWave();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::StartWave()
{
	m_waveCount++;
	m_amoutOfBotsToSpawn = 2 * m_waveCount;

	GetWorldTimerManager().SetTimer(m_timerBotSpawner, this, &ASIGameMode::SpawnBotTimerElapsed, 1.f, true, 0.f);

	SetWaveState(WaveState::WaveInProgress);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::SpawnBotTimerElapsed()
{
	SpawnNewBot();
	--m_amoutOfBotsToSpawn;
	if (m_amoutOfBotsToSpawn <= 0)
	{
		EndWave();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::EndWave()
{
	GetWorldTimerManager().ClearTimer(m_timerBotSpawner);

	SetWaveState(WaveState::WaitingToComplete);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::UpdateEnemiesArray(AActor* enemy,bool isSpawning)
{
	//bool isPreparingForWave = GetWorldTimerManager().IsTimerActive(m_timerNextWaveStart);
	//if (m_amoutOfBotsToSpawn > 0 || isPreparingForWave) return;
	if (!enemy) return;

	if (isSpawning)
	{
		m_enemies.Add(enemy);
	}
	else
	{
		m_enemies.Remove(enemy);
	}

	if (m_enemies.Num() == 0)
	{
		SetWaveState(WaveState::WaveComplete);
		PrepareNextWave();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::PrepareNextWave()
{
	GetWorldTimerManager().SetTimer(m_timerNextWaveStart, this, &ASIGameMode::StartWave,m_timeBetweenWaves, false);

	SetWaveState(WaveState::WaitingToStart);

	RestartDeadPlayers();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::CheckPlayersAlive()
{
	//@TODO create array for players and gameover when every1 is dead 
	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; ++it)
	{
		APlayerController* pc = it->Get();
		if (pc)
		{
			APawn* player = pc->GetPawn();
			if (player)
			{
				UHealthComponent* healthComp = Cast<UHealthComponent>(player->GetComponentByClass(UHealthComponent::StaticClass()));
				if (ensure(healthComp) && healthComp->GetHealth() > 0.f)
				{
					return;
				}
			}
		}
	}
	GameOver();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::GameOver()
{
	EndWave();

	SetWaveState(WaveState::GameOver);

	UE_LOG(LogTemp, Log, TEXT("Game Over LUL"));
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::SetWaveState(WaveState newState)
{
	ATPSGameState* gameState = GetGameState<ATPSGameState>();

	if (ensureAlways(gameState))
	{
		gameState->SetWaveState(newState);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASIGameMode::RestartDeadPlayers()
{
	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; ++it)
	{
		APlayerController* pc = it->Get();
		if (pc && !pc->GetPawn())
		{
			RestartPlayer(pc);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
