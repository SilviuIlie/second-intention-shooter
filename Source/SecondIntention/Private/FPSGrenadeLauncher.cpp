// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/FPSGrenadeLauncher.h"
#include "Components/SkeletalMeshComponent.h"
#include "Public/FPSGrenadeProjectile.h"
#include "Components/StaticMeshComponent.h"
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AFPSGrenadeLauncher::AFPSGrenadeLauncher()
{

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AFPSGrenadeLauncher::Fire()
{
	AActor* weaponOwner = GetOwner();
	if (!weaponOwner) return;

	FVector eyeLocation;
	FRotator eyeRotation;
	weaponOwner->GetActorEyesViewPoint(eyeLocation, eyeRotation);

	FVector fireLocation = m_meshComponent->GetSocketLocation(m_fireEffectSocketName);
	FRotator fireRotation = m_meshComponent->GetSocketRotation(m_fireEffectSocketName);

	FActorSpawnParameters spawnParameters;
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	GetWorld()->SpawnActor<AFPSGrenadeProjectile>(m_grenadeProjectile,fireLocation,eyeRotation,spawnParameters);
}
