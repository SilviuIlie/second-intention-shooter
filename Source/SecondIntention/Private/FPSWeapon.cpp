// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/FPSWeapon.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "HAL/IConsoleManager.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "SecondIntention.h"
#include "Public/TimerManager.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"

static int32 debugWeaponDrawing = 0;
static FAutoConsoleVariable CVARDebugWeaponDrawing(TEXT("Shooter.DebugWeapons"), debugWeaponDrawing, TEXT("Draw Debug Lines For Weapons"), EConsoleVariableFlags::ECVF_Cheat);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
AFPSWeapon::AFPSWeapon()
{
	m_meshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh Component"));
	RootComponent = m_meshComponent;

	m_fireEffectSocketName = "FireEffectSocket";
	m_tracerTargetName = "Target";

	m_baseDamage = 20.f;
	m_vulnerableHitModifier = 4.f;
	m_fireRate = 60.f;

	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 33.f;
	SetReplicates(true);

	m_initialBulletSpread = 3.f;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AFPSWeapon::BeginPlay()
{
	Super::BeginPlay();
	m_finalBulletSpread = m_initialBulletSpread;
	m_timeBetweenShots = 60.f / m_fireRate;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AFPSWeapon::Fire()
{
	if (Role < ROLE_Authority)
	{
		ServerFire();
	}

	AActor* weaponOwner = GetOwner();
	if (!weaponOwner) return;

	FVector eyeLocation;
	FRotator eyeRotation;
	weaponOwner->GetActorEyesViewPoint(eyeLocation, eyeRotation);

	FVector directionFromEye = eyeRotation.Vector();
	
	float weaponSprayRadians = FMath::DegreesToRadians(m_finalBulletSpread);
	directionFromEye = FMath::VRandCone(directionFromEye, weaponSprayRadians, weaponSprayRadians);

	//trace for a long distance so we can hit almost everything
	FVector traceEnd = eyeLocation + (directionFromEye * 10000);

	
	//ignore the weapon and the owner from collision . 
	//bTraceComplex is used for checking complex collision ( checking the triangle hit instead of a bounding box or other simple collision).
	FCollisionQueryParams queryParameters;
	queryParameters.AddIgnoredActor(weaponOwner);
	queryParameters.AddIgnoredActor(this);
	queryParameters.bTraceComplex = true;
	queryParameters.bReturnPhysicalMaterial = true;

	FVector traceFinalEnd = traceEnd;

	FHitResult hitResult;
	EPhysicalSurface surfaceType = SurfaceType_Default;


	// checks if we hit something 
	if (GetWorld()->LineTraceSingleByChannel(hitResult, eyeLocation, traceEnd, COLLISION_WEAPON, queryParameters))
	{
		AActor* hitActor = hitResult.GetActor();

		traceFinalEnd = hitResult.ImpactPoint;

		surfaceType = UPhysicalMaterial::DetermineSurfaceType(hitResult.PhysMaterial.Get());

		float finalDamage = m_baseDamage;
		if (surfaceType == SURFACE_FLESHVULNERABLE)
		{
			finalDamage *= m_vulnerableHitModifier;
		}

		UGameplayStatics::ApplyPointDamage(hitActor, finalDamage, directionFromEye, hitResult, weaponOwner->GetInstigatorController(), weaponOwner, m_damageType);

		PlayImpactFX(surfaceType,hitResult.ImpactPoint);
	}

	debugWeaponDrawing = CVARDebugWeaponDrawing->GetInt();
	if (debugWeaponDrawing > 0 )
	{
		DrawDebugLine(GetWorld(), eyeLocation, traceEnd, FColor::Red, false, 1.f, 0, 1.f);
	}

	PlayFireEffects(traceFinalEnd);

	if (Role == ROLE_Authority)
	{
		m_hitscanTrace.m_traceEnd = traceFinalEnd;
		m_hitscanTrace.m_surfaceType = surfaceType;
		++m_hitscanTrace.m_replicationCount;
	}

	m_lastFireTime = GetWorld()->TimeSeconds;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AFPSWeapon::ServerFire_Implementation()
{
	Fire();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool AFPSWeapon::ServerFire_Validate() // for anti cheat
{
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AFPSWeapon::StartFire()
{
	float firstDelay = FMath::Max( m_lastFireTime + m_timeBetweenShots - GetWorld()->TimeSeconds,0.f);

	GetWorldTimerManager().SetTimer(m_timerBetweenShots,this, &AFPSWeapon::Fire , m_timeBetweenShots, true, firstDelay);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AFPSWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(m_timerBetweenShots);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AFPSWeapon::PlayFireEffects(const FVector& traceEnd)
{
	if (m_fireEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(m_fireEffect, m_meshComponent, m_fireEffectSocketName);
	}

	if (m_traceEffect)
	{
		//fireLocation is the place where the trace starts (weapon end)
		FVector fireLocation = m_meshComponent->GetSocketLocation(m_fireEffectSocketName);

		UParticleSystemComponent* traceParticleComponent = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), m_traceEffect, fireLocation);

		if (traceParticleComponent)
		{
			traceParticleComponent->SetVectorParameter(m_tracerTargetName, traceEnd);
		}
	}

	APawn* myOwner = Cast<APawn>(GetOwner());
	if (myOwner)
	{
		APlayerController* pc = Cast<APlayerController>(myOwner->GetController());
		if (pc)
		{
			pc->ClientPlayCameraShake(m_fireCameraShake);
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AFPSWeapon::PlayImpactFX(EPhysicalSurface surfaceType, FVector impactPoint)
{
	UParticleSystem* finalEffect = m_defaultImpactEffect;

	switch (surfaceType)
	{
	case SURFACE_FLESHDEFAULT:
	case SURFACE_FLESHVULNERABLE:
		finalEffect = m_fleshImpactEffect;
		break;
	default:
		break;
	}

	if (finalEffect)
	{
		FVector fireLocation = m_meshComponent->GetSocketLocation(m_fireEffectSocketName);

		FVector shotDirection = impactPoint - fireLocation;
		shotDirection.Normalize();

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), finalEffect, impactPoint, shotDirection.Rotation());
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AFPSWeapon::OnRep_HitscanTrace()
{
	//cosmetic FX
	PlayFireEffects(m_hitscanTrace.m_traceEnd);

	PlayImpactFX(m_hitscanTrace.m_surfaceType, m_hitscanTrace.m_traceEnd);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AFPSWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AFPSWeapon, m_hitscanTrace, COND_SkipOwner);
}


