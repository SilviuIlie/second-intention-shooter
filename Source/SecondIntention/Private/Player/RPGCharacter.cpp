// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/Player/RPGCharacter.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "Classes/GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/Actor.h"  //used for network replication
#include "Engine/World.h"
#include "Public/FPSWeapon.h"
#include "Public/HealthComponent.h"
#include "SecondIntention.h"
#include "Net/UnrealNetwork.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ARPGCharacter::ARPGCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_springArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm Comp"));
	m_springArmComponent->bUsePawnControlRotation = true;
	m_springArmComponent->SetupAttachment(RootComponent);
	m_cameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera Component"));
	m_cameraComponent->SetupAttachment(m_springArmComponent);
	m_healthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));


	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	m_zoomedFOV = 65.f;
	m_bWantsToZoom = false;
	m_zoomInterpSpeed = 20.f;

	m_isDead = false;
	m_weaponAttachmentSocketName = "WeaponSocket";
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	m_defaultFOV = m_cameraComponent->FieldOfView;
	m_healthComp->m_onHealthChanged.AddDynamic(this, &ARPGCharacter::OnHealthChanged);

	if (Role == ROLE_Authority)
	{
		//Create Weapon
		FActorSpawnParameters spawnParams;
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		m_currentWeapon = GetWorld()->SpawnActor<AFPSWeapon>(m_starterWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, spawnParams);

		if (m_currentWeapon)
		{
			m_currentWeapon->SetOwner(this);
			m_currentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, m_weaponAttachmentSocketName);
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FVector ARPGCharacter::GetPawnViewLocation() const
{
	if (!m_cameraComponent)
	{
		return Super::GetPawnViewLocation();
	}

	return m_cameraComponent->GetComponentLocation();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::OnHealthChanged(UHealthComponent * hpComponent, float health, float healthDelta, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	if (health <= 0.f && !m_isDead)
	{
		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		m_isDead = true;

		DetachFromControllerPendingDestroy();

		StopFire();
		SetLifeSpan(10.f);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float targetFOV = m_bWantsToZoom ? m_zoomedFOV : m_defaultFOV;
	
	float currentFOV = FMath::FInterpTo(m_cameraComponent->FieldOfView, targetFOV, DeltaTime, m_zoomInterpSpeed);

	m_cameraComponent->SetFieldOfView(currentFOV);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ARPGCharacter, m_currentWeapon);
	DOREPLIFETIME(ARPGCharacter, m_isDead);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//bind movement
	PlayerInputComponent->BindAxis("MoveForward", this, &ARPGCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARPGCharacter::MoveRight);
	
	//bind camera rotation
	PlayerInputComponent->BindAxis("LookUp", this, &ARPGCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &ARPGCharacter::AddControllerYawInput);

	//bind crouch
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ARPGCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ARPGCharacter::EndCrouch);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ARPGCharacter::StartJump);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ARPGCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ARPGCharacter::StopFire);

	PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &ARPGCharacter::BeginZoom);
	PlayerInputComponent->BindAction("Zoom", IE_Released, this, &ARPGCharacter::EndZoom);

	
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::MoveForward(float directionValue)
{
	//if (m_currentWeapon)
	//{
	//	m_currentWeapon->SetBulletSpread(m_runningAmplifier);
	//}
	AddMovementInput(GetActorForwardVector() * directionValue);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::MoveRight(float directionValue)
{
	//if (m_currentWeapon)
	//{
	//	m_currentWeapon->SetBulletSpread(m_runningAmplifier);
	//}
	AddMovementInput(GetActorRightVector() * directionValue);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::BeginCrouch()
{
	if (m_currentWeapon)
	{
		m_currentWeapon->SetBulletSpread(m_crouchAmplifier);
	}
	if (!GetCharacterMovement()->IsFalling())
	{
		Crouch();
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::EndCrouch()
{
	if (m_currentWeapon)
	{
		m_currentWeapon->SetBulletSpread(1.f);
	}
	if (!GetCharacterMovement()->IsFalling())
	{
		UnCrouch();
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::StartJump()
{
	Jump();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::StartFire()
{
	if (m_currentWeapon)
	{
		m_currentWeapon->StartFire();
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::StopFire()
{
	if (m_currentWeapon)
	{
		m_currentWeapon->StopFire();
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::BeginZoom()
{
	if (m_currentWeapon)
	{
		m_currentWeapon->SetBulletSpread(m_zoomedAmplifier);
	}
	m_bWantsToZoom = true;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ARPGCharacter::EndZoom()
{
	if (m_currentWeapon)
	{
		m_currentWeapon->SetBulletSpread(1.f);
	}
	m_bWantsToZoom = false;
}
