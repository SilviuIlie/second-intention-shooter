// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/ExplosiveBarrel.h"
#include "Public/HealthComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/DamageType.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "SecondIntention.h"
#include "Net/UnrealNetwork.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AExplosiveBarrel::AExplosiveBarrel()
{
	m_meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	m_meshComponent->SetSimulatePhysics(true);
	m_meshComponent->SetCanEverAffectNavigation(false);
	m_meshComponent->SetCollisionObjectType(ECC_PhysicsBody);
	RootComponent = m_meshComponent;

	m_healthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));

	m_radialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("Radial Force Component"));
	m_radialForceComponent->bImpulseVelChange = true;
	m_radialForceComponent->bAutoActivate = false;
	m_radialForceComponent->bIgnoreOwningActor = true;
	m_radialForceComponent->SetupAttachment(m_meshComponent);
	m_damage = 100.f;
	m_damageRadius = 200.f;
	m_didExploded = false;
	m_explosionImpulse = 10.f;

	SetReplicates(true);
	SetReplicateMovement(true);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AExplosiveBarrel::BeginPlay()
{
	Super::BeginPlay();

	m_healthComp->m_onHealthChanged.AddDynamic(this, &AExplosiveBarrel::OnHealthChanged);
	m_radialForceComponent->Radius = m_damageRadius;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AExplosiveBarrel::OnHealthChanged(UHealthComponent* hpComponent, float health, float healthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (m_didExploded) return;

	if (health <= 0)
	{
		m_didExploded = true;
		OnRep_Exploded();
		Explode();
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AExplosiveBarrel::Explode()
{
	UGameplayStatics::ApplyRadialDamage(GetWorld(), m_damage, GetActorLocation(), m_damageRadius, UDamageType::StaticClass(), TArray<AActor*>(), this, (AController*)GetOwner(), ECC_Visibility);
	
	m_meshComponent->AddImpulse(FVector::UpVector * m_explosionImpulse, NAME_None, true);
	m_radialForceComponent->FireImpulse();	
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AExplosiveBarrel::OnRep_Exploded()
{
	//cosmetics
	if (m_explosionEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), m_explosionEffect, GetActorLocation());
	}
	if (m_explodedMaterial)
	{
		m_meshComponent->SetMaterial(0, m_explodedMaterial);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AExplosiveBarrel::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AExplosiveBarrel, m_didExploded);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////