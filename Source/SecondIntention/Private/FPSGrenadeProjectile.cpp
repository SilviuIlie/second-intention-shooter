// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/FPSGrenadeProjectile.h"

#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"

#include "GameFramework/DamageType.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "Public/TimerManager.h"
#include "Public/DrawDebugHelpers.h"

#include "Kismet/GameplayStatics.h"

#include "UObject/ConstructorHelpers.h"

#include "Particles/ParticleSystemComponent.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

AFPSGrenadeProjectile::AFPSGrenadeProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_sphereCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Component"));
	m_sphereCollisionComponent->InitSphereRadius(5.f);
	m_sphereCollisionComponent->BodyInstance.SetCollisionProfileName("Projectile");

	// can't step on it
	m_sphereCollisionComponent->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	m_sphereCollisionComponent->CanCharacterStepUpOn = ECB_No;

	RootComponent = m_sphereCollisionComponent;

	m_meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mest Component"));;

	static ConstructorHelpers::FObjectFinder<UStaticMesh>sphereMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	m_meshComponent->SetStaticMesh(sphereMeshAsset.Object);

	m_meshComponent->AttachTo(m_sphereCollisionComponent);

	m_projectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	m_projectileMovementComponent->UpdatedComponent = m_sphereCollisionComponent;
	m_projectileMovementComponent->InitialSpeed = 3000.f;
	m_projectileMovementComponent->MaxSpeed = 3000.f;
	m_projectileMovementComponent->bRotationFollowsVelocity = true;
	m_projectileMovementComponent->bShouldBounce = true;

	m_delayTime = 1.f;
	m_damage = 50.f;
	m_damageRadius = 200.f;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AFPSGrenadeProjectile::BeginPlay()
{
	Super::BeginPlay();
	
	FTimerHandle timerHandle;
	GetWorldTimerManager().SetTimer(timerHandle, this, &AFPSGrenadeProjectile::Explode, m_delayTime);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AFPSGrenadeProjectile::Explode()
{
	if (m_explosioneffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), m_explosioneffect, GetActorLocation());
	}

	UGameplayStatics::ApplyRadialDamage(GetWorld(), m_damage, GetActorLocation(), m_damageRadius, UDamageType::StaticClass(), TArray<AActor*>(), this, GetInstigatorController(), ECC_Visibility);
	DrawDebugSphere(GetWorld(), GetActorLocation(), m_damageRadius, 10, FColor::FColor(230, 200, 0), false, 1.f,0,1.f);
	Destroy();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
