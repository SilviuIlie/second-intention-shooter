// Fill out your copyright notice in the Description page of Project Settings.

#include "TPSGameState.h"
#include "Net/UnrealNetwork.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ATPSGameState::OnRep_WaveState(WaveState oldState)
{
	WaveStateChanged(m_waveState, oldState);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ATPSGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ATPSGameState, m_waveState);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ATPSGameState::SetWaveState(WaveState newState)
{
	if (Role == ROLE_Authority)
	{
		WaveState oldState = m_waveState;

		m_waveState = newState;
		OnRep_WaveState(oldState);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////