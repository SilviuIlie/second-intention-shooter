// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/HealthComponent.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "Engine/World.h"
#include "SIGameMode.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

UHealthComponent::UHealthComponent()
{
	m_isDead = false;
	m_defaultHealth = 100.f;
	m_teamNumber = 255;
	SetIsReplicated(true);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UHealthComponent::Heal(float healAmount)
{
	if (healAmount <= 0.f || m_health <= 0.f)	return;

	m_health = FMath::Clamp(m_health + healAmount, 0.f, m_defaultHealth);
	UE_LOG(LogTemp, Warning, TEXT("Health : %f (+ %f)"), m_health,healAmount);
	m_onHealthChanged.Broadcast(this, m_health, -healAmount, nullptr, nullptr, nullptr);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool UHealthComponent::IsFriendly(AActor * actorA, AActor * actorB)
{
	if (!actorA || !actorB) return true;
	UHealthComponent* healthComponentA = Cast<UHealthComponent>(actorA->GetComponentByClass(UHealthComponent::StaticClass()));
	UHealthComponent* healthComponentB = Cast<UHealthComponent>(actorB->GetComponentByClass(UHealthComponent::StaticClass()));
	if (!healthComponentA || !healthComponentA) return true;

	return healthComponentA->m_teamNumber == healthComponentB->m_teamNumber;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// happens only on server
	if (GetOwnerRole() == ROLE_Authority) 
	{
		AActor* owner = GetOwner();
		if (owner)
		{
			owner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);
		}
	}

	m_health = m_defaultHealth;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UHealthComponent::OnRep_Health(float oldHealth)
{
	m_onHealthChanged.Broadcast(this, m_health, m_health - oldHealth, nullptr, nullptr, nullptr);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UHealthComponent::TakeDamage(AActor * DamagedActor, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	if (Damage <= 0 || m_isDead)  return;
	
	if (DamagedActor != DamageCauser && IsFriendly(DamagedActor, DamageCauser)) return;

	m_health = FMath::Max(m_health - Damage, 0.f);

	UE_LOG(LogTemp, Warning, TEXT("Health : %f"), m_health);

	m_onHealthChanged.Broadcast(this, m_health, Damage, DamageType, InstigatedBy, DamageCauser);

	if (m_health <= 0)
	{
		m_isDead = true;
		ASIGameMode* gameMode = Cast<ASIGameMode>(GetWorld()->GetAuthGameMode());
		if (gameMode)
		{
			gameMode->m_onActorKilled.Broadcast(GetOwner(), DamageCauser, InstigatedBy);
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, m_health);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
