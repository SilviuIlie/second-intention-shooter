// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/Powerup.h"
#include "Public/TimerManager.h"
#include "Components/StaticMeshComponent.h"
#include "Components/PointLightComponent.h"
#include "Net/UnrealNetwork.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
APowerup::APowerup()
{
	PrimaryActorTick.bCanEverTick = true;

	m_dummy = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy"));
	RootComponent = m_dummy;

	m_meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	m_meshComponent->SetSimulatePhysics(false);
	m_meshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	m_meshComponent->AttachTo(m_dummy);

	m_pointLightComponent = CreateDefaultSubobject<UPointLightComponent>(TEXT("Point Light Component"));
	m_pointLightComponent->AttachTo(m_meshComponent);

	m_powerupInterval = 0.f;
	m_totalNumberOfTicks = 0.f;
	m_rotationSpeed = 15.f;

	m_isPowerupActive = false;

	SetReplicates(true);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void APowerup::ActivatePowerup(AActor* activatedBy)
{
	OnActivate(activatedBy);

	m_isPowerupActive = true;
	
	OnRep_PowerupActive();

	if (m_powerupInterval > 0.f)
	{
		GetWorldTimerManager().SetTimer(m_timerPowerupTick, this, &APowerup::PowerupTick, m_powerupInterval, true);
	}
	else
	{
		PowerupTick();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void APowerup::OnRep_PowerupActive()
{
	if (m_isPowerupActive)
	{
		m_meshComponent->SetVisibility(false, true);
	}
	else
	{

	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void APowerup::PowerupTick()
{
	++m_totalTicksProcessed;

	OnPowerupTicked();

	if (m_totalTicksProcessed >= m_totalNumberOfTicks)
	{
		OnExpire();
		m_isPowerupActive = false;
		GetWorldTimerManager().ClearTimer(m_timerPowerupTick);
		Destroy();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void APowerup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	m_meshComponent->AddLocalRotation(FRotator(0.f, m_rotationSpeed * DeltaTime, 0.f));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void APowerup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APowerup, m_isPowerupActive);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////