// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Powerup.generated.h"

class UPointLightComponent;

UCLASS()
class SECONDINTENTION_API APowerup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APowerup();

protected:
	
	UFUNCTION()
	void PowerupTick();

	UFUNCTION()
	void OnRep_PowerupActive();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USceneComponent* m_dummy;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* m_meshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UPointLightComponent* m_pointLightComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	float m_rotationSpeed;


	UPROPERTY(EditDefaultsOnly,Category = "Powerup")
	float m_powerupInterval;

	UPROPERTY(EditDefaultsOnly, Category = "Powerup")
	int32 m_totalNumberOfTicks;

	UPROPERTY(ReplicatedUsing = OnRep_PowerupActive)
	bool m_isPowerupActive;

	int32 m_totalTicksProcessed;

	FTimerHandle m_timerPowerupTick;
public:	
	virtual void Tick(float DeltaTime) override;

	void ActivatePowerup(AActor* activatedBy);

	UFUNCTION(BlueprintImplementableEvent,Category = "Powerup")
	void OnActivate(AActor* activatedBy);

	UFUNCTION(BlueprintImplementableEvent, Category = "Powerup")
	void OnPowerupTicked();

	UFUNCTION(BlueprintImplementableEvent, Category = "Powerup")
	void OnExpire();
};
