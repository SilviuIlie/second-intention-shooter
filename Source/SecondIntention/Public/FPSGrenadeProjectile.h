// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSGrenadeProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UStaticMeshComponent;
class UParticleSystem;

UCLASS()
class SECONDINTENTION_API AFPSGrenadeProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSGrenadeProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	UParticleSystem* m_explosioneffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	float m_delayTime;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	float m_damageRadius;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	float m_damage;

	void Explode();
private:
	UPROPERTY(VisibleDefaultsOnly, Category = "Projectile")
	UStaticMeshComponent* m_meshComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = "Projectile")
	USphereComponent* m_sphereCollisionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* m_projectileMovementComponent;

	
};
