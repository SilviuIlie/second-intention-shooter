// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSWeapon.generated.h"

class UDamageType;
class UParticleSystem;
class USkeletalMeshComponent;

USTRUCT()
struct FHitScanTrace  //info about a weapon linetrace
{
	GENERATED_BODY()
public:
	UPROPERTY()
	TEnumAsByte<EPhysicalSurface> m_surfaceType;
	UPROPERTY()
	FVector_NetQuantize m_traceEnd;
	UPROPERTY()
	uint8 m_replicationCount;
};


UCLASS()
class SECONDINTENTION_API AFPSWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSWeapon();

	// Fire from the pawn eyes to crosshair location
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void Fire();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire();

	void StartFire();

	void StopFire();

	void SetBulletSpread(float amplifier) { m_finalBulletSpread = amplifier * m_initialBulletSpread; }
protected:

	virtual void BeginPlay() override;

	void PlayFireEffects(const FVector& traceEnd);
	
	void PlayImpactFX(EPhysicalSurface surfaceType,FVector impactPoint);

	FName m_tracerTargetName;

	UFUNCTION()
	void OnRep_HitscanTrace();
	///////////////////////////////////////////////////////////////////////////////////
	/*Fire Effects*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* m_fireEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* m_defaultImpactEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* m_fleshImpactEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* m_traceEffect;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	FName m_fireEffectSocketName;
	
	///////////////////////////////////////////////////////////////////////////////////
	/*Damage*/
	float m_timeBetweenShots;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	float m_fireRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	float m_baseDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	float m_vulnerableHitModifier;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly ,Category = "Damage")
	TSubclassOf<UDamageType> m_damageType;

	///////////////////////////////////////////////////////////////////////////////////
	/*Camera Effects*/
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<UCameraShake> m_fireCameraShake;

	///////////////////////////////////////////////////////////////////////////////////
	/*degrees*/
	UPROPERTY(EditDefaultsOnly, Category = "Weapon", meta = (ClampMin =0.f))
	float m_initialBulletSpread;

	float m_finalBulletSpread;

	FTimerHandle m_timerBetweenShots;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* m_meshComponent;

	UPROPERTY(ReplicatedUsing = OnRep_HitscanTrace)
	FHitScanTrace m_hitscanTrace;

private:
	float m_lastFireTime;
};
