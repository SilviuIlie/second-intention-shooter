// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FPSWeapon.h"
#include "FPSGrenadeLauncher.generated.h"

class AFPSGrenadeProjectile;

/**
 * 
 */
UCLASS()
class SECONDINTENTION_API AFPSGrenadeLauncher : public AFPSWeapon
{
	GENERATED_BODY()
public:
	AFPSGrenadeLauncher();
	//UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void Fire() override;
protected:
	UPROPERTY(EditDefaultsOnly, Category = "Projectile")
	TSubclassOf<AFPSGrenadeProjectile> m_grenadeProjectile;
};
