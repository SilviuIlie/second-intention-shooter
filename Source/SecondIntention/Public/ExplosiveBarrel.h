// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplosiveBarrel.generated.h"

class UHealthComponent;
class UStaticMeshComponent;
class UMaterial;
class UParticleSystem;
class URadialForceComponent;

UCLASS()
class SECONDINTENTION_API AExplosiveBarrel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExplosiveBarrel();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHealthChanged(UHealthComponent* hpComponent, float health, float healthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	void Explode();

	UHealthComponent* m_healthComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* m_meshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	URadialForceComponent* m_radialForceComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	UMaterial* m_explodedMaterial;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	UParticleSystem* m_explosionEffect;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*Damage*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	float m_damageRadius;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	float m_damage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	float m_explosionImpulse;

	UPROPERTY(ReplicatedUsing=OnRep_Exploded)
	bool m_didExploded;

	UFUNCTION()
	void OnRep_Exploded();
};
