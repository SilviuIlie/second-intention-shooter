// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Delegates/Delegate.h"
#include "SIGameMode.generated.h"

enum class WaveState : uint8;

class UEnvQuery;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnActorKilled, AActor*, VictimActor, AActor*, KillerActor, AController*, killerController);

UCLASS()
class SECONDINTENTION_API ASIGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASIGameMode();

	virtual void StartPlay() override;
	
	UPROPERTY(BlueprintAssignable, Category = "GameMode")
	FOnActorKilled m_onActorKilled;
protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void SpawnNewBot();
	/*
	isSpawning is true -> insert into array
	isSpawning is false -> delete from array
	*/
	UFUNCTION(BlueprintCallable, Category = "EnemyWave")
	void UpdateEnemiesArray(AActor* enemy, bool isSpawning);

	void SpawnBotTimerElapsed();
	
	void StartWave();
	void EndWave();
	void PrepareNextWave();

	void CheckPlayersAlive();
	void GameOver();

	void SetWaveState(WaveState newState);

	void RestartDeadPlayers();

	TArray<AActor*> m_enemies;

	int32 m_waveCount = 0;
	int32 m_amoutOfBotsToSpawn;

	FTimerHandle m_timerNextWaveStart;
	FTimerHandle m_timerBotSpawner;

	UPROPERTY()
	UEnvQuery* m_test;

	UPROPERTY(EditDefaultsOnly,Category = "GameMode")
	float m_timeBetweenWaves;
};
