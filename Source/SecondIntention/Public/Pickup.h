// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

class UDecalComponent;
class APowerup;
class USphereComponent;

UCLASS()
class SECONDINTENTION_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

protected:
	virtual void BeginPlay() override;

	void Spawn();

	UPROPERTY(VisibleAnywhere, Category = "Component")
	UDecalComponent* m_decalComponent;
	UPROPERTY(VisibleAnywhere, Category = "Component")
	USphereComponent* m_sphereComponent;

	APowerup* m_powerupInstance;
	UPROPERTY(EditInstanceOnly, Category = "Pickup")
	TSubclassOf<APowerup> m_powerupClass;
	UPROPERTY(EditInstanceOnly, Category = "Pickup")
	float m_cooldown;

	FTimerHandle m_timerRespawn;

public:	
	virtual void NotifyActorBeginOverlap(AActor* otherActor) override;
};
