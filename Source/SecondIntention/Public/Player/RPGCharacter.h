// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RPGCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class AFPSWeapon;
class UHealthComponent;

UCLASS()
class SECONDINTENTION_API ARPGCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARPGCharacter();


	UFUNCTION(BlueprintCallable, Category = "Player")
	void StartFire();

	UFUNCTION(BlueprintCallable, Category = "Player")
	void StopFire();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//direction value is clamped between -1 and 1
	void MoveForward(float directionValue);
	void MoveRight(float directionValue);

	void BeginCrouch();
	void EndCrouch();

	void StartJump();

	void BeginZoom();
	void EndZoom();

	UFUNCTION()
	void OnHealthChanged(UHealthComponent* hpComponent, float health, float healthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* m_springArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* m_cameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UHealthComponent* m_healthComp;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Player")
	bool m_isDead;

	bool m_bWantsToZoom;
	float m_defaultFOV;

	UPROPERTY(EditDefaultsOnly, Category = "CameraZoom")
	float m_zoomedFOV;
	UPROPERTY(EditDefaultsOnly, Category = "CameraZoom" , meta = (ClampMin =0.1, ClampMax =100.0))
	float m_zoomInterpSpeed;

	UPROPERTY(Replicated)
	AFPSWeapon* m_currentWeapon;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<AFPSWeapon> m_starterWeaponClass;
	
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName m_weaponAttachmentSocketName;

	/*bullet spread amplifiers*/
	UPROPERTY(EditDefaultsOnly, Category = "BulletSpreadAmplifiers")
	float m_runningAmplifier = 3.f;
	UPROPERTY(EditDefaultsOnly, Category = "BulletSpreadAmplifiers")
	float m_crouchAmplifier = .2f;
	UPROPERTY(EditDefaultsOnly, Category = "BulletSpreadAmplifiers")
	float m_standingAmplifier = 1.f;
	UPROPERTY(EditDefaultsOnly, Category = "BulletSpreadAmplifiers")
	float m_zoomedAmplifier = .3f;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override;
};
